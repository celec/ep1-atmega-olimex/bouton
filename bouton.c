#include <avr/io.h>
#include "declarations.h" // affiche & init & timer

int main(void){
	init();
	timer_init();
	int x = 0;
	char sens = 1;

	while(1){
		if (TIFR1 & (1 << OCF1A)){
			if ((PINB & (1<<PINB7)) != 0x80) {
				sens ^= 1; // changement de sens	
				for (int i = 0; i < 10; ++i) {affiche(i,0);}
			}
			TIFR1 |= (1 << OCF1A); // clear flag
			if (sens) { // sens positif
				if (x > 9) {x = 0;}
				if (x != 0) {affiche(x-1, 0);} 
				else {affiche(9, 0);}
				affiche(x, 1);
				x++;
			} else { // sens inverse
				if (x < 0) {x = 9;}
				if (x != 9) {affiche(x+1, 0);}
				else {affiche(0, 0);}
				affiche(x, 1);
				x--;
			}
		}
	}
	return 0;
}
