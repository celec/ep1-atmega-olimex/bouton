#include <avr/io.h>
#define F_CPU 16000000UL
#define delai_leds (50000) // prescale de 64 → 5Hz ou 200ms

void affiche(int i, int e){
	unsigned char broche[10] = {2,3,1,0,4,6,7,6,4,5}; //n of PORTXn
	unsigned char port[10] = {2,2,2,2,2,1,2,3,0,0}; // B=0, then +3

	if (e) {*(unsigned char *)(0x25 + 3*port[i]) |= 1<<broche[i];}
	else {*(unsigned char *)(0x25 + 3*port[i]) &= ~(1<<broche[i]);}
}

void init(void) {
	unsigned char broche[10] = {2,3,1,0,4,6,7,6,4,5}; //n of PORTXn
	unsigned char port[10] = {2,2,2,2,2,1,2,3,0,0}; // B=0, then +3
	
	// Déclarations des registres
	for (int i = 0; i < 10; ++i) { // sorties et mise à 0
		*(unsigned char *)(0x24 + 3*port[i]) |= 1<<broche[i];
		*(unsigned char *)(0x25 + 3*port[i]) &= ~(1<<broche[i]);
	}

	// Bouton
	DDRB &= ~(1<<PORTB7); // D11
	PORTB |= 1<<PORTB7; // D11 en Pull-Up
	PINB &= ~(1<<PORTB7); // Init du bouton à 0
}

/* timer 16 bits: overflow avec Δt = prescale / F_CPU → T = 0xFFFF * Δt
 *                CTC avec même Δt → T = OCRnA * Δt
 *                      → OCRnA = F_CPU / (Freq_voulue * prescale)
 */
void timer_init(void){  // Timer1 avec prescaler=64 et CTC (WGM=2)
	TCCR1B |= (1 << WGM12)|(1 << CS11)|(1 << CS10);
	OCR1A = delai_leds; // comparaison CTC
}
